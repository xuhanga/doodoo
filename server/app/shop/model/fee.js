const fee_sku = require("./fee_sku");
const fee_sku_address = require("./fee_sku_address");

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "shop_deliveryfee",
    hasTimestamps: true,
    fee_sku: function() {
        return this.hasMany(fee_sku, "deliveryfee_id");
    },
    address_fee: function() {
        return this.hasMany(fee_sku_address, "deliveryfee_id");
    }
});
