const WXAuth = require("./../../../../lib/wxa/wxa.class");
const base = require("./base");

module.exports = class extends base {
    async _initialize() {
        await super._initialize();
    }

    /**
     *
     * @api {post} /api/public/wxa_login 小程序授权登录
     * @apiDescription 小程序授权登录
     * @apiGroup App Api
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} Referer 小程序referer
     *
     * @apiParam {String} code 授权code
     * @apiParam {String} encryptedData 授权encryptedData
     * @apiParam {String} iv 授权iv
     *
     * @apiSuccess {JSON} success 用户数据
     *
     * @apiSampleRequest /api/public/wxa_login
     *
     */
    async wxaLogin() {
        const { code, encryptedData, iv } = this.post;
        if (!code || !encryptedData || !iv) {
            this.fail("小程序授权登录参数有误");
            return;
        }

        if (!this.state.wxa) {
            this.fail("系统错误");
            return;
        }

        const wxaAuth = new WXAuth(
            process.env.OPEN_APPID,
            process.env.OPEN_APPSECRET
        );
        const session = await wxaAuth.jscode2session(
            this.state.wxa.authorizer_appid,
            code
        );
        const sessionKey = session.session_key;
        if (!sessionKey) {
            this.fail(session, "小程序授权登录，code获取session失败");
            return;
        }

        const userInfo = await wxaAuth.getUserInfo(
            this.state.wxa.authorizer_appid,
            sessionKey,
            encryptedData,
            iv
        );

        const ip = this.get("X-Real-IP") || this.ip;
        const data = {
            openId: userInfo.openId,
            nickName: userInfo.nickName,
            gender: userInfo.gender,
            language: userInfo.language,
            city: userInfo.city,
            province: userInfo.province,
            country: userInfo.country,
            avatarUrl: userInfo.avatarUrl
        };

        let user = await this.model("user")
            .query({
                where: {
                    openId: userInfo.openId
                }
            })
            .fetch();
        if (!user) {
            user = await this.model("user")
                .forge(
                    Object.assign(
                        {
                            wxa_id: this.state.wxa.id
                        },
                        data
                    )
                )
                .save();

            this.hook.run("wxaAnalysis", "user", this.state.wxa.id, 1);
            this.hook.run("analysis", "user");
        } else {
            // 更新用户数据
            await this.model("user")
                .forge(Object.assign({ id: user.id }, data))
                .save();
        }

        const token = this.jwtSign({
            id: user.id,
            wxa_id: user.wxa_id
        });
        this.success({
            token: token,
            user: user,
            watermark: userInfo.watermark
        });
    }
};
