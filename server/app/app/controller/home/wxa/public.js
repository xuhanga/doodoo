const fse = require("fs-extra");
const base = require("./../base");
const WXAuth = require("./../../../../../lib/wxa/wxa.class");

module.exports = class extends base {
    async _initialize() {
        await super.isAppAuth();
    }

    /**
     *
     * @api {get} /home/wxa/public/login 发起小程序授权链接
     * @apiDescription 发起小程序授权链接
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiParam {String} AppToken 应用授权token
     * @apiParam {String} redirect 跳转链接
     * @apiParam {String} auth_code auth_code
     *
     * @apiSuccess {String} redirect 跳转到微信授权页面
     *
     * @apiSampleRequest /home/wxa/public/login
     *
     */
    async login() {
        const { AppToken, redirect, auth_code } = this.query;
        const wxauth = new WXAuth(
            process.env.OPEN_APPID,
            process.env.OPEN_APPSECRET
        );
        if (auth_code) {
            const app_id = this.state.app.id;
            const authToken = await wxauth.getAuthToken(auth_code);
            const authInfo = await wxauth.getAuthInfo(
                authToken.authorization_info.authorizer_appid
            );
            const data = {
                nick_name: authInfo.authorizer_info.nick_name,
                head_img: authInfo.authorizer_info.head_img,
                user_name: authInfo.authorizer_info.user_name,
                qrcode_url: authInfo.authorizer_info.qrcode_url,
                principal_name: authInfo.authorizer_info.principal_name,
                signature: authInfo.authorizer_info.signature,
                request_domain: authInfo.authorizer_info.MiniProgramInfo.network.RequestDomain.join(),
                wsrequest_domain: authInfo.authorizer_info.MiniProgramInfo.network.WsRequestDomain.join(),
                upload_domain: authInfo.authorizer_info.MiniProgramInfo.network.UploadDomain.join(),
                download_domain: authInfo.authorizer_info.MiniProgramInfo.network.DownloadDomain.join(),
                authorizer_appid: authInfo.authorization_info.authorizer_appid,
                authorizer_access_token:
                    authToken.authorization_info.authorizer_access_token,
                expires_in: authToken.authorization_info.expires_in,
                expires_at: authToken.authorization_info.expires_at,
                authorizer_refresh_token:
                    authToken.authorization_info.authorizer_refresh_token,
                func_info: JSON.stringify(
                    authInfo.authorization_info.func_info
                ),
                verify_type_info: JSON.stringify(
                    authInfo.authorizer_info.verify_type_info
                )
            };

            // 移除当前应用绑定的小程序
            let wxa = await this.model("wxa")
                .query({
                    where: {
                        app_id: app_id
                    }
                })
                .fetch();
            if (wxa) {
                await this.model("wxa")
                    .forge({
                        id: wxa.id,
                        app_id: 0
                    })
                    .save();
            }

            wxa = await this.model("wxa")
                .query({
                    where: {
                        authorizer_appid:
                            authInfo.authorization_info.authorizer_appid
                    }
                })
                .fetch();
            if (!wxa) {
                wxa = await this.model("wxa")
                    .forge(
                        Object.assign(data, {
                            app_id
                        })
                    )
                    .save();
                // 授权统计
                this.hook.run("analysis", "wxa");
            } else {
                await this.model("wxa")
                    .forge(
                        Object.assign(
                            {
                                id: wxa.id,
                                app_id,
                                authorized: 1
                            },
                            data
                        )
                    )
                    .save();
            }

            this.redirect(redirect);
        } else {
            const redirect_uri =
                process.env.APP_HOST +
                `/app/home/wxa/public/login?AppToken=${AppToken}&redirect=${encodeURIComponent(
                    redirect
                )}`;
            const url = await wxauth.getComponentOauthUrl(redirect_uri);
            this.redirect(url);
        }
    }

    /**
     *
     * @api {get} /home/wxa/public/templateWxa 获取小程序模版列表
     * @apiDescription 获取小程序模版列表
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiHeader {String} AppToken 应用授权token.
     *
     * @apiParam {Number} template_id 模版id
     *
     * @apiSampleRequest /home/wxa/public/templateWxa
     */
    async templateWxa() {
        const { template_id } = this.query;
        const templates = await this.model("template_wxa")
            .query(qb => {
                qb.where("template_id", template_id);
                qb.orderBy("rank", "desc");
            })
            .fetchAll();
        this.success(templates);
    }

    /**
     *
     * @api {get} /home/wxa/public/loginReferer 伪造referer
     * @apiDescription 伪造referer
     * @apiGroup App Home
     * @apiVersion 0.0.1
     *
     * @apiParam {String} AppToken 应用授权token
     * @apiParam {String} redirect 跳转链接
     *
     * @apiSampleRequest /home/wxa/public/loginReferer
     */
    async loginReferer() {
        const { AppToken, redirect } = this.query;
        const url =
            process.env.APP_HOST +
            `/app/home/wxa/public/login?AppToken=${AppToken}&redirect=${encodeURIComponent(
                redirect
            )}`;
        const html = await fse.readFile("data/referer/index.html");
        this.view(this.strToJsTemplate(html, { url }));
    }
};
