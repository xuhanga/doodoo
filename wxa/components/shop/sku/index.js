const app = getApp();
Component({
    properties: {
        detailInfo: Object
    },
    data: {
        color: wx.getExtConfigSync().color
    },
    attached: function() {
        // console.log(this)
    },
    methods: {
        selectSpec(e) {
            const item = e.currentTarget.dataset.item;
            const list = e.currentTarget.dataset.list;
            const detailInfo = this.data.detailInfo;
            if (detailInfo.sku_status && detailInfo.skuData.length) {
                detailInfo.skuData.forEach(val => {
                    if (val.id == item.id) {
                        val.child.forEach(sku => {
                            if (sku.id == list.id) {
                                sku.is_select = 1;
                            } else {
                                sku.is_select = 0;
                            }
                        })
                    }
                })
                let sku_ids = [];
                detailInfo.skuData.forEach(val => {
                    if (val.child && val.child.length) {
                        val.child.forEach(sku => {
                            if (sku.is_select) {
                                sku_ids.push(sku.id);
                            }
                        })
                    }
                })
                sku_ids = sku_ids.join('-');
                let select_sku = {};
                detailInfo.sku.forEach(val => {
                    if (val.sku_ids == sku_ids) {
                        select_sku = val;
                    }
                })
                detailInfo.select_sku = select_sku;
            }
            this.setData({
                detailInfo: detailInfo
            })
        },
        bindinput(e) {
            let num = e.detail.value;
            if (num.length == 1) {
                num = num.replace(/[^1-9]/g, '0')
            } else {
                num = num.replace(/\D/g, '')
            }
            this.data.detailInfo.num = ~~num ? ~~num : 1;
            this.setData({
                detailInfo: this.data.detailInfo
            })
        },
        changeNum(e) {
            const type = Number(e.currentTarget.id);
            if (type) {
                this.data.detailInfo.num++;
            } else {
                if (this.data.detailInfo.num > 1) {
                    this.data.detailInfo.num--;
                }
            }
            this.triggerEvent('changeNum', this.data.detailInfo);
            this.setData({
                detailInfo: this.data.detailInfo
            })
        },
        buyNowOrAddCart() {
            this.triggerEvent('buyNowOrAddCart', this.data.detailInfo);
        }
    }
});