// pages/order/order-list/index.js
Page({
    data: {
        color: wx.getExtConfigSync().color,
        orderOperate: ["全部", "待付款", "待发货", "待收货", "已完成"],
        orderOperateIndex: 0,
        page: 1,
        orderlist: [],
        is_all: 0
    },
    onLoad: function(options) {
        wx.removeStorageSync('order-type');
        // 0：代付款 1：代发货 2：待收货 3：退换货(从个人中心四个订单按钮跳转过来)
        if (options.type) {
            this.setData({
                type: Number(options.type)
            })
        }
    },
    onShow: function() {
        this.changeTab();
    },
    changeTab(e) {
        let index = e ? Number(e.currentTarget.dataset.index) : 0;
        if (e) {
            this.setData({
                type: 'undefined'
            })
        }
        if (index == 0) {
            this.data.status = '';
            this.data.pay_status = '';
        }
        if (index == 1 || this.data.type == 0) {
            this.data.status = 0;
            this.data.pay_status = 0;
            index = 1;
        }
        if (index == 2 || this.data.type == 1) {
            this.data.status = 0;
            this.data.pay_status = 1;
            index = 2;
        }
        if (index == 3 || this.data.type == 2) {
            this.data.status = 1;
            this.data.pay_status = '';
            index = 3;
        }
        if (index == 4) {
            this.data.status = 2;
            this.data.pay_status = '';
        }
        if (this.data.type == 3) {
            // 退换货
            this.data.status = 2;
            this.data.pay_status = 1;
        }
        if (wx.getStorageSync('order-type')) {
            const obj = wx.getStorageSync('order-type');
            this.data.status = obj.status;
            this.data.pay_status = obj.pay_status;
            index = obj.index;
            wx.removeStorageSync('order-type');
        }
        this.setData({
            page: 1,
            orderlist: [],
            orderOperateIndex: index
        })
        this.getOrderList();
    },
    getOrderList() {
        let orderlist = this.data.orderlist;
        wx.doodoo.fetch(`/shop/api/shop/order/order/index`, {
            method: "GET",
            data: {
                page: this.data.page,
                status: this.data.status,
                pay_status: this.data.pay_status
            }
        }).then(res => {
            if (res && res.data.errmsg == 'ok') {
                this.data.page++;
                const DATA = res.data.data.data;
                orderlist = orderlist.concat(DATA);
                if (!DATA.length || !orderlist.length) this.data.is_all = 1;
                this.setData({
                    page: this.data.page,
                    is_all: this.data.is_all,
                    orderlist: orderlist
                });
            }
        })
    },
    goDetail(e) {
        wx.setStorageSync('order-type', {
            status: this.data.status,
            pay_status: this.data.pay_status,
            index: this.data.orderOperateIndex
        });
        wx.navigateTo({
            url: `../order-detail/index?order_id=${e.currentTarget.dataset.id}`
        })
    },
    operate(e) {
        const type = Number(e.currentTarget.id);
        switch (type) {
            case 0: // 确认收货
                {
                    const order_id = e.detail.value.order_id;
                    wx.doodoo.fetch(`/shop/api/shop/order/order/update`, {
                        method: 'GET',
                        data: {
                            id: order_id,
                            status: 2
                        }
                    }).then(res => {
                        if (res && res.data.errmsg == 'ok') {
                            this.setData({
                                page: 1,
                                orderlist: []
                            })
                            this.getOrderList();
                        }
                    })
                    break;
                };
            case 1: // 付款
                {
                    const order_id = e.currentTarget.dataset.order_id;
                    const trade_id = e.detail.value.trade_id;
                    wx.doodoo.fetch(`/app/pay/wxpay/index?id=${trade_id}`).then(res => {
                        if (res && res.data.errmsg == 'ok') {
                            let response = res.data.data;
                            let timeStamp = response.timeStamp;
                            //可以支付
                            wx.requestPayment({
                                timeStamp: timeStamp.toString(),
                                nonceStr: response.nonceStr,
                                package: response.package,
                                signType: response.signType,
                                paySign: response.paySign,
                                success: () => {
                                    this.send_tplmsg(response.package, order_id); // 支付成功，发送支付成功模板
                                    this.setData({
                                        page: 1,
                                        orderlist: []
                                    })
                                    this.getOrderList();
                                }
                            });
                        } else {
                            wx.showModal({
                                title: "提示",
                                content: res.data.data,
                                showCancel: false
                            });
                        }
                    });
                    break;
                };
            case 2: // 取消订单
                {
                    const order_id = e.detail.value.order_id;
                    const that = this;
                    wx.showModal({
                        title: "提示",
                        content: "确定取消订单吗？",
                        success: function(res) {
                            if (res.confirm) {
                                wx.doodoo.fetch(`/shop/api/shop/order/order/update`, {
                                    method: 'GET',
                                    data: {
                                        id: order_id,
                                        status: -1
                                    }
                                }).then(res => {
                                    if (res && res.data.errmsg == 'ok') {
                                        that.setData({
                                            page: 1,
                                            orderlist: []
                                        })
                                        that.getOrderList();
                                    }
                                })
                            } else if (res.cancel) {
                                console.log("用户点击取消");
                            }
                        }
                    })
                    break;
                };
            case 3: // 查看物流
                {
                    wx.setStorageSync('order-type', {
                        status: this.data.status,
                        pay_status: this.data.pay_status,
                        index: this.data.orderOperateIndex
                    });
                    const order = e.currentTarget.dataset.data;
                    wx.navigateTo({
                        url: `../logistics/index?order=${JSON.stringify(order)}`
                    })
                    break;
                };
        }
    },
    // 发送模板消息
    send_tplmsg: function(formId, order_id) {
        formId = formId.substring(10);
        wx.doodoo.fetch("/api/order/sendPayTpl", {
            method: "GET",
            data: {
                formId: formId,
                orderId: order_id
            }
        }, res => {
            console.log("发送模板消息", res);
        });
    },
    onReachBottom: function() {
        if (!this.data.is_all) {
            this.getOrderList();
        }
    },
    onPullDownRefresh: function() {
        setTimeout(() => {
            this.setData({
                page: 1,
                orderlist: [],
                is_all: 0
            })
            this.getOrderList();
            wx.stopPullDownRefresh();
        }, 500)
    }
})